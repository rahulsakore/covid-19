package com.covid.tracker.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.covid.tracker.models.Records;
import com.covid.tracker.services.CovidDataService;

@Controller
public class HomeController {
	
	@Autowired
	CovidDataService CDS;
	@GetMapping("/")
	public String home(Model model) {
		List<Records> currentRecords = CDS.getCurrentRecords();
		int totalCases = currentRecords.stream().mapToInt(stat -> stat.getTotalCases()).sum();
		int totalNewCases = currentRecords.stream().mapToInt(stat -> stat.getPrevDayCases()).sum();
		model.addAttribute("records",CDS.getCurrentRecords());
		model.addAttribute("totalCases",totalCases);
		model.addAttribute("totalNewCases",totalNewCases);
		return "home";
	}
	
	
}
