package com.covid.tracker.models;

public class Records {

	private String state;
	private String country;
	private int totalCases;
	private int prevDayCases;
	
	
	public int getPrevDayCases() {
		return prevDayCases;
	}
	public void setPrevDayCases(int prevDayCases) {
		this.prevDayCases = prevDayCases;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getTotalCases() {
		return totalCases;
	}
	public void setTotalCases(int totalCases) {
		this.totalCases = totalCases;
	}
	@Override
	public String toString() {
		return "Records [state=" + state + ", country=" + country + ", totalCases=" + totalCases + "]";
	}
	
	
	
	
}
