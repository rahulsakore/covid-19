package com.covid.tracker.services;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.*;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

import com.covid.tracker.models.Records;

@Service
public class CovidDataService {

	private static String GIT_URL = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv";
	private List<Records> currentRecords = new ArrayList<>();
	
	public List<Records> getCurrentRecords() {
		return currentRecords;
	}
	
	@PostConstruct
	public void getData() throws IOException, Exception {
		List<Records> newRecords = new ArrayList<>();
		
		
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(GIT_URL))
				.build();
		HttpResponse<String> httpResponse = client.send(request, HttpResponse.BodyHandlers.ofString());
//		System.out.println(httpResponse.body()); 
		
		
		StringReader csvFile = new StringReader(httpResponse.body());
		Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(csvFile);
		
		for (CSVRecord record : records) {
			Records getRecords = new Records();
			
		    getRecords.setState(record.get("Province/State"));
		    getRecords.setCountry(record.get("Country/Region"));
		    int currentCases = Integer.parseInt(record.get(record.size()-1));
		    int prevDayCases = Integer.parseInt(record.get(record.size()-2));
		    getRecords.setTotalCases(currentCases); 
		    getRecords.setPrevDayCases(currentCases-prevDayCases);
//		    System.out.println(getRecords);
		    newRecords.add(getRecords);
		}
		
		this.currentRecords = newRecords;
//		System.out.println("this is current" + currentRecords.get(1));
		       
	}
	
	

	
}
